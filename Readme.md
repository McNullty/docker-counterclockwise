# Counterclockwise IDE

IDE for Clojure development

## Usage

```bash

xhost +

docker run -ti --rm -v ~/wspace/:/home/developer/workspace/ -v ~/.m2:/home/developer/.m2 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix counterclockwise


```

Make sure that directory wspace already exists, because if it doesn't docker will create it with root ownership and image will not be able to use it as workspace.