#!/bin/bash

echo "Binding port 3000 to port 3080"

# Scipt for running IDE
xhost +

docker run -ti --rm -p 3080:3000 -v ~/wspace2/:/home/developer/workspace/ -v ~/.m2:/home/developer/.m2 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix counterclockwise
